const express = require('express')
const mongoose = require('mongoose')
require('dotenv').config() // Initialize ENV file
const app = express()
const port = 3001

//Mongoose connection

mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.qb1qgfz.mongodb.net/b271-todo-db?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// The 'db' variable represents the connection which is established after running the 'connect()' function
let db = mongoose.connection 

// Listening for events in the connection. The "error" event will trigger only if the connection has an error. And the "open" event will trigger only if it has successfully connected to MongoDB.
db.on("error", () => console.log("Connection error"))
db.once("open", ()=> console.log("Connected to MongoDB!"))

// Schema

const user_schema = new mongoose.Schema({
	username: String,
	password: String
})

//  Model

const User = mongoose.model("User", user_schema)



// Middleware registration
app.use(express.json())
app.use(express.urlencoded({extended:true}))

// [SECTION] Routes
// Register new user
app.post('/signup', (request, response) => {
	User.findOne({username: request.body.username}).then((result, error) => {
		// Checks if the username is already existing in the database
		if(result !== null && result.username == request.body.username){
			return response.send("Duplicate user found!")
		} else { // If there are no duplicates, proceed with the registration of the user

			// 1. Create a new instance of the 'User' model containing properties required based on the schema of that model
			let new_user = new User({
				username: request.body.username,
				password: request.body.password
			})

			// 2. Save that new instance of the 'User' model into the database and handle any errors accordingly
			new_user.save().then((registered_user, error) => {
				if(error){
					return console.log(error)
				} else {
					return response.status(201).send("New user registered")
				}
			})
		}
	})
})

app.get('/users', (request, response) => {
	User.find({}).then((result, error) => {
		if(error){
			return console.log(error)
		}
		return response.status(200).send(result)
	})
})


app.listen(port, () => console.log(`The server is running at localhost: ${port}`))

